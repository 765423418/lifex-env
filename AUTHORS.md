# Authors

The following people (in alphabetical order) have contributed to lifex-env,
with many more who have reported bugs or sent in suggestions, fixes or small enhancements:

- Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>

# candi

The origin is candi, licensed under the
  GNU Lesser General Public License Version 3,
from
  https://github.com/dealii/candi

Please find the list of the authors of candi below:
- Uwe Koecher    <uwe.koecher@hsu-hamburg.de>
- Bruno Turcksin <bruno.turcksin@gmail.com>
- Timo Heister   <timo.heister@gmail.com>
