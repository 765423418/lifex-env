# <img alt="lifex" width="150" src="https://gitlab.com/lifex/lifex/-/raw/master/doc/logo/lifex.png" />-env

The `lifex-env.sh` shell scripts download, configure, build, and install
**<kbd>life<sup>x</sup></kbd>** dependencies on `Linux`-based systems.

It is recommended that the following packages are already installed
in the system before running `lifex-env.sh`. They often come pre-compiled as modules
on HPC clusters or are available through the package manager of your
`Linux`-based system (such as `apt`, `yum`, `brew`, ...).

- [`BLAS`](http://www.netlib.org/blas/)/[`LAPACK`](http://www.netlib.org/lapack/).

If they are not already available, modify the configuration file
[lifex-env.cfg](lifex-env.cfg) to select and install them, as explained below.

## Quickstart

Run these commands and follow the instructions on the screen:

```bash
git clone https://gitlab.com/lifex/lifex-env.git
cd lifex-env
./lifex-env.sh [options]
```

The list of all available options is available through the `--help` flag:
```bash
./lifex-env.sh --help
```

Customize the options, such as `-j<N>`, according to your needs.
The installation may take a long time to complete.

Once done, remember to run the following command
(or add it to your `${HOME}/.bashrc` file or equivalent)
in order to prepare the environment:

```bash
source /path/to/lifex-env/configuration/enable_lifex.sh
```

> **Note**: the `enable_lifex.sh` script needs to be loaded also
> in case you want to resume a previously interrupted installation
> or to install new packages in a second time.

## Configuration file

The installation of any package can be enabled/disabled in the configuration file
[lifex-env.cfg](lifex-env.cfg).

The complete list of packages is available in
[lifex-toolchain/packages](lifex-toolchain/packages).

There are several other options within the configuration file, such as

- the `DOWNLOAD_PATH` directory where to download source archives (can be safely removed after installation),
- the `SRC_PATH` directory where to download or unpack source files (can be safely removed after installation),
- the `BUILD_PATH` directory where to build packages (can be safely removed after installation),
- the `INSTALL_PATH` directory where to install packages,

and more.

## Platform files

Specific instructions and environment settings are available in the
[lifex-toolchain/platforms](lifex-toolchain/platforms) folder for a number
of different platforms.

`life<sup>x</sup>-env` tries to automatically detect the platform
it is running on.

Nevertheless, a specific platform file can be forced through the
`--platform` flag, *e.g.*

```bash
./lifex-env.sh --platform=lifex-toolchain/platforms/linux_cluster
```
